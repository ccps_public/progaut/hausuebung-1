#include "allocledger.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>
#include <tuple>
#include <vector>

static std::vector<std::pair<void*, size_t>> f_alloc_table{};

void* malloc_tracked(size_t size) {
    void* p = malloc(size);
    f_alloc_table.push_back(std::make_pair(p, size));
    return p;
}

void* calloc_tracked(size_t num, size_t size) {
    void* p = calloc(num, size);
    f_alloc_table.push_back(std::make_pair(p, num * size));
    return p;
}

void free_tracked(void* p) {
    const auto it = std::find_if(f_alloc_table.begin(), f_alloc_table.end(),
                                 [p](const auto& item) { return (item.first == p); });

    assert(it != f_alloc_table.end());
    f_alloc_table.erase(it);

    free(p);
}

void alloc_ledger_reset() { f_alloc_table.clear(); }

void alloc_ledger_report() {
    if (f_alloc_table.empty()) {
        return;
    }

    std::cout << "Alloc report begin *************** \n";
    for (auto alloc : f_alloc_table) {
        std::cout << "   " << alloc.second << " bytes leaked from one allocation.\n";
    }
    std::cout << "Alloc report end ***************** \n";
}

size_t alloc_ledger_get_leaked_bytes() {
    return std::accumulate(f_alloc_table.cbegin(), f_alloc_table.cend(), 0,
                           [](size_t acc, auto alloc) { return acc + alloc.second; });
}
