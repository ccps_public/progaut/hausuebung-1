#include "testhelper.hpp"

#include <gtest/gtest.h>

void compareMatrix(const matrix_t* test, const matrix_t* correct) {
    EXPECT_EQ(test->cols, correct->cols);
    EXPECT_EQ(test->rows, correct->rows);
    for (unsigned int i = 0; i < test->rows * test->cols; ++i) {
        EXPECT_NEAR(test->data[i], correct->data[i], 0.01);
    }
}