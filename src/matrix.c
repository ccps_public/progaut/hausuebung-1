#include "matrix.h"

#include <stdio.h>
#include <stdlib.h>

matrix_t createMatrix_uninit(unsigned int rows, unsigned int cols) {

    matrix_t a = {0, 0, NULL};
    return a;
}

matrix_t createMatrix_init(unsigned int rows, unsigned int cols,
                           double* initdata) {

    matrix_t a = {0, 0, NULL};
    return a;
}

matrix_t zerosMatrix(unsigned int rows, unsigned int cols) {
    matrix_t a = {0, 0, NULL};
    return a;
}

matrix_t identityMatrix(unsigned int n) {
    matrix_t a = {0, 0, NULL};
    return a;
}

double getMatrixElement(const matrix_t* a, unsigned int i, unsigned int j) {
    return 0;
}

void setMatrixElement(matrix_t* a, unsigned int i, unsigned int j,
                      double value) {
}

void freeMatrix(matrix_t* a) {
}

void printMatrix(const matrix_t* a) {
}

matrix_t addMatrix(const matrix_t* a, const matrix_t* b) {
    matrix_t r = {0, 0, NULL};
    return r;
}

matrix_t scaleMatrix(const matrix_t* a, double factor) {
    matrix_t r = {0, 0, NULL};
    return r;
}

matrix_t subtractMatrix(const matrix_t* a, const matrix_t* b) {
    matrix_t r = {0, 0, NULL};
    return r;
}

matrix_t transposeMatrix(const matrix_t* a) {
    matrix_t r = {0, 0, NULL};
    return r;
}

matrix_t multiplyMatrix(const matrix_t* a, const matrix_t* b) {
    matrix_t r = {0, 0, NULL};
    return r;
}

double determinantMatrix(const matrix_t* a) {
    return 0;
}

matrix_t adjugateMatrix(const matrix_t* a) {
    matrix_t r = {0, 0, NULL};
    return r;
}

matrix_t inverseMatrix(const matrix_t* a) {
    matrix_t r = {0, 0, NULL};
    return r;
}
