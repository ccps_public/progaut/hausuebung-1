#ifndef ALLOCLEDGER_H
#define ALLOCLEDGER_H

extern "C" {
#include "allocledger_use.h"
}

void alloc_ledger_reset();
void alloc_ledger_report();
size_t alloc_ledger_get_leaked_bytes();

#endif  // ALLOCLEDGER_H
