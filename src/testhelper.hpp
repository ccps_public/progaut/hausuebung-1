//
// Created by lucas on 02.11.23.
//
extern "C" {
#include "matrix.h"
}
#ifndef HAUSUEBUNG1_TESTHELPER_H
#define HAUSUEBUNG1_TESTHELPER_H

void compareMatrix(const matrix_t* test, const matrix_t* correct);

#endif  // HAUSUEBUNG1_TESTHELPER_H
