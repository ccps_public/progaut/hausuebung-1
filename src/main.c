#include <math.h>
#include <stdio.h>

#include "matrix.h"

int main() {
    double dataA[] = {-2, 2, 0, 2, -4, 2, 0, 2, -3};
    double dataB[] = {0.5, 0, 0, 0, 0, 0.5};
    double dataC[] = {0, 1, 0, 0, 0, 1};
    matrix_t A = createMatrix_init(3, 3, dataA);
    matrix_t B = createMatrix_init(3, 2, dataB);
    matrix_t C = createMatrix_init(2, 3, dataC);

    if (fabs(determinantMatrix(&A)) < 1e-10) {
        printf("matrix A has no inverse");
    } else {
        matrix_t Ainv = inverseMatrix(&A);
        matrix_t CAinv = multiplyMatrix(&C, &Ainv);
        matrix_t CAinvB = multiplyMatrix(&CAinv, &B);
        matrix_t gain = scaleMatrix(&CAinvB, -1);

        printf("dcgain matrix:\n");
        printMatrix(&gain);

        freeMatrix(&Ainv);
        freeMatrix(&CAinv);
        freeMatrix(&CAinvB);
        freeMatrix(&gain);
    }

    freeMatrix(&A);
    freeMatrix(&B);
    freeMatrix(&C);

    // double data[] = {1, 2, 7, 4, 5, 6, 7, 8, 9};
    // matrix_t test = createMatrix_init(3, 3, data);
    // printf("det = %f\n", determinantMatrix(&test));
    // freeMatrix(&test);
    return 0;
}
