extern "C" {
#include "matrix.h"
}
#include <gtest/gtest.h>

#include "allocledger.hpp"
#include "testhelper.hpp"

TEST(Matrix, create_uninit) {
    alloc_ledger_reset();
    matrix_t test = createMatrix_uninit(5, 7);

    EXPECT_EQ(test.rows, 5);
    EXPECT_EQ(test.cols, 7);
    EXPECT_NE(test.data, nullptr);

    freeMatrix(&test);

    EXPECT_EQ(test.data, nullptr);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, create_init) {
    alloc_ledger_reset();
    double data[] = {1, 2, 3, 4, 5, 6};
    matrix_t test = createMatrix_init(3, 2, data);

    EXPECT_EQ(test.rows, 3);
    EXPECT_EQ(test.cols, 2);
    EXPECT_NE(test.data, nullptr);

    for (int i = 0; i < 3 * 2; ++i) {
        EXPECT_EQ(test.data[i], data[i]);
    }

    freeMatrix(&test);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, zeros) {
    alloc_ledger_reset();
    matrix_t test = zerosMatrix(4, 2);

    EXPECT_EQ(test.rows, 4);
    EXPECT_EQ(test.cols, 2);

    for (int i = 0; i < 4 * 2; ++i) {
        EXPECT_EQ(test.data[i], 0.0);
    }

    freeMatrix(&test);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, identity) {
    alloc_ledger_reset();
    matrix_t test = identityMatrix(3);
    double data[] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
    matrix_t correct = createMatrix_init(3, 3, data);
    compareMatrix(&test, &correct);

    freeMatrix(&test);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, get_element) {
    alloc_ledger_reset();
    double data[] = {1, 2, 3, 4, 5, 6};
    matrix_t test = createMatrix_init(2, 3, data);

    EXPECT_EQ(getMatrixElement(&test, 0, 0), 1.0);
    EXPECT_EQ(getMatrixElement(&test, 1, 0), 4.0);
    EXPECT_EQ(getMatrixElement(&test, 0, 1), 2.0);

    freeMatrix(&test);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, set_element) {
    alloc_ledger_reset();
    double data[] = {1, 2, 3, 4, 5, 6};
    matrix_t test = createMatrix_init(2, 3, data);

    setMatrixElement(&test, 0, 0, 10.0);
    setMatrixElement(&test, 1, 0, 40.0);
    setMatrixElement(&test, 0, 1, 20.0);

    double data_correct[] = {10, 20, 3, 40, 5, 6};
    matrix_t correct = createMatrix_init(2, 3, data_correct);

    compareMatrix(&test, &correct);

    freeMatrix(&test);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, add) {
    alloc_ledger_reset();
    double id3[] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
    matrix_t identityMat = createMatrix_init(3, 3, id3);
    matrix_t test = addMatrix(&identityMat, &identityMat);

    double data[] = {2, 0, 0, 0, 2, 0, 0, 0, 2};
    matrix_t correct = createMatrix_init(3, 3, data);
    compareMatrix(&test, &correct);

    freeMatrix(&test);
    freeMatrix(&identityMat);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, subtract) {
    alloc_ledger_reset();
    double id3[] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
    matrix_t identity = createMatrix_init(3, 3, id3);
    matrix_t test = subtractMatrix(&identity, &identity);

    double data[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    matrix_t correct = createMatrix_init(3, 3, data);
    compareMatrix(&test, &correct);

    freeMatrix(&test);
    freeMatrix(&identity);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, scale) {
    alloc_ledger_reset();
    double id3[] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
    matrix_t identityMat = createMatrix_init(3, 3, id3);
    matrix_t test = scaleMatrix(&identityMat, 2.0);

    double data[] = {2, 0, 0, 0, 2, 0, 0, 0, 2};
    matrix_t correct = createMatrix_init(3, 3, data);
    compareMatrix(&test, &correct);

    freeMatrix(&test);
    freeMatrix(&identityMat);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, transpose_non_symmetric) {
    alloc_ledger_reset();
    double data[] = {1, 0, 0, 0, 1, 42, 0, 0, 1};
    matrix_t test = createMatrix_init(3, 3, data);
    matrix_t test_transposed = transposeMatrix(&test);
    freeMatrix(&test);

    double data_correct[] = {1, 0, 0, 0, 1, 0, 0, 42, 1};
    matrix_t correct = createMatrix_init(3, 3, data_correct);
    compareMatrix(&test_transposed, &correct);

    freeMatrix(&test_transposed);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, multiplyMatrix) {
    alloc_ledger_reset();
    double dataA[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    double dataB[] = {10, 20, 30, 40, 50, 60, 70, 80, 90};

    matrix_t testA = createMatrix_init(3, 3, dataA);
    matrix_t testB = createMatrix_init(3, 3, dataB);
    matrix_t testAB = multiplyMatrix(&testA, &testB);
    freeMatrix(&testA);
    freeMatrix(&testB);

    double data_correct[] = {300, 360, 420, 660, 810, 960, 1020, 1260, 1500};
    matrix_t correct = createMatrix_init(3, 3, data_correct);
    compareMatrix(&testAB, &correct);

    freeMatrix(&testAB);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, determinant_2x2) {
    alloc_ledger_reset();
    double data1[] = {1, 2, -4, -8};
    matrix_t test1 = createMatrix_init(2, 2, data1);

    EXPECT_NEAR(determinantMatrix(&test1), 0.0, 1e-12);

    double data2[] = {1, 2, 3, 4};
    matrix_t test2 = createMatrix_init(2, 2, data2);

    EXPECT_NEAR(determinantMatrix(&test2), -2.0, 1e-12);

    freeMatrix(&test1);
    freeMatrix(&test2);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, determinant_3x3) {
    alloc_ledger_reset();
    double data1[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    matrix_t test1 = createMatrix_init(3, 3, data1);

    EXPECT_NEAR(determinantMatrix(&test1), 0.0, 1e-12);

    double data2[] = {1, 1, 1, 1, 1, 2, 2, 1, 1};
    matrix_t test2 = createMatrix_init(3, 3, data2);

    EXPECT_NEAR(determinantMatrix(&test2), 1.0, 1e-12);

    freeMatrix(&test1);
    freeMatrix(&test2);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, adjugate_1x1) {
    alloc_ledger_reset();
    matrix_t test = zerosMatrix(1, 1);
    matrix_t test_adj = adjugateMatrix(&test);
    freeMatrix(&test);

    double data_correct[] = {1};
    matrix_t correct = createMatrix_init(1, 1, data_correct);
    compareMatrix(&test_adj, &correct);

    freeMatrix(&test_adj);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, adjugate_2x2) {
    alloc_ledger_reset();
    double data[] = {1, 2, 3, 4};

    matrix_t test = createMatrix_init(2, 2, data);
    matrix_t test_adj = adjugateMatrix(&test);
    freeMatrix(&test);

    double data_correct[] = {4, -2, -3, 1};
    matrix_t correct = createMatrix_init(2, 2, data_correct);
    compareMatrix(&test_adj, &correct);

    freeMatrix(&test_adj);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, adjugate_3x3) {
    alloc_ledger_reset();
    double data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    matrix_t test = createMatrix_init(3, 3, data);
    matrix_t test_adj = adjugateMatrix(&test);
    freeMatrix(&test);

    double data_correct[] = {-3, 6, -3, 6, -12, 6, -3, 6, -3};
    matrix_t correct = createMatrix_init(3, 3, data_correct);
    compareMatrix(&test_adj, &correct);

    freeMatrix(&test_adj);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, inverse_2x2) {
    alloc_ledger_reset();
    double data[] = {1, 2, 3, 4};

    matrix_t test = createMatrix_init(2, 2, data);
    matrix_t test_inv = inverseMatrix(&test);
    freeMatrix(&test);

    double data_correct[] = {-2, 1, 1.5, -0.5};
    matrix_t correct = createMatrix_init(2, 2, data_correct);
    compareMatrix(&test_inv, &correct);

    freeMatrix(&test_inv);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}

TEST(Matrix, inverse_3x3) {
    alloc_ledger_reset();
    double data[] = {1, 2, 0, 0, 1, 2, 0, 0, 1};

    matrix_t test = createMatrix_init(3, 3, data);
    matrix_t test_inv = inverseMatrix(&test);
    freeMatrix(&test);

    double data_correct[] = {1, -2, 4, 0, 1, -2, 0, 0, 1};
    matrix_t correct = createMatrix_init(3, 3, data_correct);
    compareMatrix(&test_inv, &correct);

    freeMatrix(&test_inv);
    freeMatrix(&correct);
    EXPECT_EQ(alloc_ledger_get_leaked_bytes(), 0);
}
