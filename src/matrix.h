#ifndef HAUSUEBUNG1_MATRIX_H
#define HAUSUEBUNG1_MATRIX_H

#include "allocledger_use.h"
#include "stdbool.h"

typedef struct matrix_ {
    unsigned int rows;
    unsigned int cols;
    double* data;
} matrix_t;

matrix_t createMatrix_uninit(unsigned int rows, unsigned int cols);
matrix_t createMatrix_init(unsigned int rows, unsigned int cols, double* initdata);
matrix_t zerosMatrix(unsigned int rows, unsigned int cols);
matrix_t identityMatrix(unsigned int n);
double getMatrixElement(const matrix_t* a, unsigned int i, unsigned int j);
void setMatrixElement(matrix_t* a, unsigned int i, unsigned int j, double value);
void freeMatrix(matrix_t* a);
void printMatrix(const matrix_t* a);

matrix_t addMatrix(const matrix_t* a, const matrix_t* b);
matrix_t scaleMatrix(const matrix_t* a, double factor);
matrix_t subtractMatrix(const matrix_t* a, const matrix_t* b);
matrix_t transposeMatrix(const matrix_t* a);

matrix_t multiplyMatrix(const matrix_t* a, const matrix_t* b);
double determinantMatrix(const matrix_t* a);
matrix_t adjugateMatrix(const matrix_t* a);
matrix_t inverseMatrix(const matrix_t* a);

#endif  // HAUSUEBUNG1_MATRIX_H
