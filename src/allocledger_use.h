#ifndef ALLOCLEDGER_USE_H
#define ALLOCLEDGER_USE_H

#include <stdlib.h>

void* malloc_tracked(size_t size);
void* calloc_tracked(size_t num, size_t size);
void free_tracked(void* p);

#endif  // ALLOCLEDGER_USE_H
